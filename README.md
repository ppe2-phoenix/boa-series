# BAO Series


# 2022/2023-L8TI003-P Program. et projet encadré 2

## Membres du groupe: 
Kenza AHMIA, Amina DJARFI

## Responsables du cours
Pierre Magistry / Marine Wauquier (contact : pierre.magistry@inalco.fr)


## Descriptif du cours

Mise en oeuvre d'une chaîne de traitement textuel semi-automatique, depuis la récupération des données jusqu'à leur présentation.
Ce cours posera d'abord la question des objectifs linguistiques à atteindre (lexicologie, recherche d'information, traduction...) et fera appel aux méthodes et outils informatiques nécessaires à leur réalisation (récupération de corpus, normalisation des textes, segmentation, étiquetage, extraction, structuration et présentation des résultats...).


# Installation


1. Clonez le dépôt Git sur votre ordinateur.
2. Il est recommandé de créer un environnement virtuel de test en utilisant la commande suivante : 
    ```Bash
    python3 -m venv myenv
    ```
3. Activez l'environnement virtuel en utilisant la commande:
    ```Bash
    source myenv/bin/activate
    ```
4. Accédez au répertoire du projet cloné et exécutez le code en suivant les instructions



# Prérequis

Installez les packages requis en utilisant la commande:

```Bash
pip install -r requirements.txt
```


# BàO 1: Extraction de données RSS vers format XML
Le script `/Scripts/extraire_tout.py` permet d'extraire des données à partir d'un fichier ou d'un répertoire de fichiers RSS et de les convertir en format XML ou JSON.

## Prérequis
Le script est écrit en Python 3 et utilise les modules suivants :

* argparse
* os
* pathlib
* re
* datetime

Ces modules doivent être installés pour pouvoir utiliser le script.

## Utilisation
Le script peut être utilisé en ligne de commande en utilisant la syntaxe suivante :



```Bash
python3 Scripts/extraire_tout.py [-h] [-d [PATH]] [-c [CATEGORIES]] [-t [TAGS]] [-sd [START_DATE]] [-ed [END_DATE]] [-p [PARSER]] [-o [OUTPUT]] [-f [FORMAT]]
```


Les options disponibles sont :

* -h, --help : Affiche l'aide et les options disponibles
* -d, --path : Le fichier RSS ou le répertoire contenant les fichiers à traiter (par défaut : 'Corpus/2022/')
* -c, --categories : La liste des catégories à traiter (par défaut : toutes les catégories sont traitées)
* -t, --tags : Les balises RSS à extraire (par défaut : titre, description et pubDate)
* -sd, --start_date : La date de début de la période à extraire (par défaut : '2022-01-01')
* -ed, --end_date : La date de fin de la période à extraire (par défaut : '2022-12-31')
* -p, --parser : La méthode de parsing à utiliser (par défaut : 'etree')
* -o, --output : Le fichier de sortie (par défaut : stdout)
* -f, --format : Le format de sortie (par défaut : 'xml')

## Exemple d'utilisation
Extraire les données de la catégorie 'sports' du dossier corpus 'Corpus/2022' et les enregistrer dans un fichier XML nommé 'output.xml' :

```bash
python3 Scripts/extraire_tout.py -d Corpus/2022 -c "societe|economie" -o bao1_output.xml -f xml -sd 2022-01-01 -ed 2022-01-15

```



# BàO 2: Enrichissement des données
Le script `/Scripts/analyse_corpus.py` permet l'analyse du fichier XML du corpus généré par BàO 1.

## Prérequis
Le script est écrit en Python 3 et utilise les modules suivants :

* argparse
* os
* pathlib
* spaCy ( modèle fr_core_news_sm)
* xml


Ces modules et modèles doivent être installés pour pouvoir utiliser le script.

## Utilisation
Le script peut être utilisé en ligne de commande en utilisant la syntaxe suivante :



```Bash
python3 Scripts/analyse_corpus.py [-h] [-p [PATH]] [-o [OUTPUT]]
```


Les options disponibles sont :

* -h, --help : Affiche l'aide et les options disponibles
* -p, --path : Le fichier XML contenant le corpus à traiter
* -o, --output : Le fichier de sortie (par défaut : stdout)


## Exemple d'utilisation
Analyse du fichier 'input.xml' et les enregistrer dans un fichier XML nommé 'output.xml' :

```bash
python3 Scripts/analyse_corpus.py -p bao1_output.xml -o bao2_output.xml

```


# BàO 3: Topic Modelings
Le script `/Scripts/run_lda.py` permet l'entrainement du modèle LDA sur un corpus généré par BàO 2.

## Prérequis
Le script est écrit en Python 3 et utilise les modules suivants :

* argparse
* os
* pathlib
* gensim
* logging
* pandas
* xml


Ces modules et modèles doivent être installés pour pouvoir utiliser le script.

## Utilisation
Le script peut être utilisé en ligne de commande en utilisant la syntaxe suivante :


```Bash
run_lda.py [-h] [-p [PATH]] [-nt [NUM_TOPICS]] [-i [ITERATIONS]] [-ps [PASSES]] [-sw] [-np [NO_POS]] [-v]

Topic modeling avec Gensim

options:
  -h, --help            show this help message and exit
  -p [PATH], --path [PATH] : le corpus XML à traiter
  -nt [NUM_TOPICS], --num_topics [NUM_TOPICS] : nombre de topics (10 par défaut)
  -i [ITERATIONS], --iterations [ITERATIONS] : nombre d'itérations (100 par défaut)
  -ps [PASSES], --passes [PASSES] : nombre de passes (10 par défaut)
  -sw, --stop_words : suppression des stop words (False par défaut)
  -np [NO_POS], --no_pos [NO_POS] : liste des POS à supprimer (None par défaut)
  -v, --verbose : afficher les messages de logging

```


## Exemple d'utilisation
Analyse du fichier 'input.xml' et les enregistrer dans un fichier XML nommé 'output.xml' :

```bash
python3 Scripts/run_lda.py -p bao2_output.xml -nt 10 -sw -np "VERB|AUX" -v

```

# BàO 4: Visualisation des résultats
Le script `/Scripts/vis_lda.py` permet la visualisation des résultats du modèle LDA généré par BàO 3.

## Utilisation
Le script peut être utilisé en ligne de commande en utilisant la syntaxe suivante :


```Bash
usage: lda_vis.py [-h] [-p [PATH]] [-c] [-v] [-o [OUTPUT]]

Topic visualization avec pyLDAvis

options:
  -h, --help: show this help message and exit
  -p [PATH], --path [PATH]: model LDA à visualiser (par défaut: models/))
  -c, --coherence: afficher la cohérence des topics (False par défaut)
  -v, --verbose: afficher les messages de logging
  -o [OUTPUT], --output [OUTPUT]: nom du fichier de sortie (par défaut: myvisLDA.html)
```


## Exemple d'utilisation

```bash
python3 Scripts/vis_lda.py -o results.html -v 
```
