#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BàO 2: enrichir les données

1 - Analyse automatique
2 - Extraction de patrons

"""

from typing import Dict, List
#import timeit as ti
import spacy

from datetime import datetime

import xml.etree.ElementTree as ET
from spacy.lang.fr.stop_words import STOP_WORDS


def nlp_load():
    """Charger le modèle de langue et désactiver les pipelines inutiles

    returns:
        nlp: modèle de langue chargé

    """
    disable = ["ner", "vectors", "senter", "textcat"]
    nlp = spacy.load("fr_core_news_sm", disable=disable)

    return nlp


# calcule le temps chargement du modèle
#start = ti.default_timer()
nlp = nlp_load()
#stop = ti.default_timer()
#print('le temps chargement du modèle: ', stop - start)


def process_text(text: str) -> List[Dict[str, str]]:
    """
    Analyser un texte avec le modèle de langue

    args:
        text: texte à analyser

    returns:
        liste de dictionnaires contenant les informations sur chaque token

    """
    text = str(text).replace('’', "'").replace('\t', ' ')

    # Désactiver les pipelines inutiles
    disable = ["ner", "vectors", "senter", "textcat"]
    doc = []
    data = {'form': '', 'lemma': '', 'pos': '', 'is_stop': ''}
    for token in nlp(text, disable=disable):
        data['form'] = token.text
        data['lemma'] = token.lemma_
        data['pos'] = token.pos_
        if token.lemma_ in STOP_WORDS or token.text in STOP_WORDS \
                or token.is_stop or token.is_punct or token.is_space \
                or token.is_digit:
            data['is_stop'] = 'true'
        else:
            data['is_stop'] = 'false'

        doc.append(data)
        data = {'form': '', 'lemma': '', 'pos': '', 'is_stop': ''}

    return doc


def process_xml(path_file, analyzer: callable = process_text,  output=None):
    """
    Charger le fichier XML du corpus

    args:
        filename: nom du fichier XML

    returns:
        list of dict: liste de token

    """

    # Charger le fichier XML
    try:
        tree = ET.parse(path_file)
    except ET.ParseError as e:
        print("erreur de parsing", e)
        return None

    root = tree.getroot()

    out_root = ET.Element("corpus")

    for file in root.findall('file'):
        out_file = ET.SubElement(out_root, "file", file.attrib)
        for article in file.findall('article'):
            out_article = ET.SubElement(out_file, "article", article.attrib)
            for title in article.findall('.//title'):

                for token in analyzer(title.text):
                    ET.SubElement(out_article, "element", token)
            for description in article.findall('.//description'):

                for token in analyzer(description.text):
                    ET.SubElement(out_article, "element", token)
            # print(art)

    tree = ET.ElementTree(out_root)

    if output is not None:
        tree.write(output, encoding='utf-8', xml_declaration=True)
    else:
        ET.dump(out_root)


def main():
    """Fonction principale"""

    """
    text = "Les trois outils vont produire des résultats similaires, \
    mais dans des structures de données différentes spécifiques à chaque outil. Les résultats de l'analyse"

    # Analyser le texte avec le modèle de langue
    for token in process_text(text.strip()):
        print(token)
        for key, value in token.items():
            print(f'{key}: {value}')
        print('------------------')

    #exit()
    """
    path_file = 'corpusTopic.xml'

    timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')

    outpath = f'output_{str(timestamp)}.xml'

    # Utilisation de joblib pour mettre en cache le modèle de langue

    # with open(outpath, 'w') as out:

    # Appel de la fonction avec le chemin du fichier XML
    process_xml(path_file, output=outpath)


if __name__ == '__main__':
    main()
