#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
import sys,os
import re
from pathlib import Path
from bao1 import re_parse_rss


def main():
    parser = argparse.ArgumentParser( description="Extraction des données d'un fichier rss vers stdout")
    parser.add_argument("-p",'--path' ,nargs='?',  default='Corpus/sample.xml', help="le fichier RSS ou le répertoire contenant les fichiers à traiter")  
     
    args = parser.parse_args()

    
    if args.path:
        
        path_files = args.path

        #test si le chemin existe
        if os.path.exists(path_files):
            
            path_file = Path(path_files)
           
        else:
            parser.error(f"le répertoire ou le fichier '{args.path}' n'existe pas")
    


    print("======================================")
    print("test re_parse_rss")

    for item in re_parse_rss(path_file):
        for tag,content in item.items():
            print(f'{tag}:{content}')
        print("--------------------------------")





    sys.exit() 



if __name__ == '__main__':
    
    main()