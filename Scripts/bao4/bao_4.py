#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BàO 4: visualisation

1 - mise en forme des sorties du modèle
2 - export pour le rapport web

"""
import logging
from gensim.models import LdaModel
from gensim import corpora





class Corpus(object):

    def deserialize(path):
        """
        Désérialiser le corpus Gensim à partir d'un fichier binaire.

        Args:
            path (str): Chemin du fichier binaire du corpus sérialisé.

        Returns:
            gensim.corpora.bleicorpus.BleiCorpus: Corpus Gensim.
        """
        try:
            corpus = corpora.BleiCorpus(path)
            logging.info("Corpus Gensim désérialisé avec succès.")
            return corpus
        except Exception as e:
            logging.error(
                "Erreur lors de la désérialisation du corpus Gensim:", e)
            return None


def load_dictionary(dictionary_path: str):
    """
    Charger un dictionnaire Gensim.
    Args:
        dictionary_path (str): Chemin vers le fichier contenant le dictionnaire Gensim.

    Returns:
        gensim.corpora.dictionary.Dictionary: Dictionnaire Gensim.
    """
    try:
        dictionary = corpora.Dictionary.load(dictionary_path)
        logging.info("Dictionnaire Gensim chargé avec succès.")
        return dictionary
    except Exception as e:
        logging.error("Erreur lors du chargement du dictionnaire Gensim:", e)
        return None



def load_model(model_path: str):
    """
    Charger un modèle LDA.
    Args:
        model_path (str): Chemin vers le fichier contenant le modèle LDA.

    Returns:
        gensim.models.ldamodel.LdaModel: Modèle LDA.
    """
    try:
        lda_model = LdaModel.load(model_path)
        logging.info("Modèle LDA chargé avec succès.")
        return lda_model
    except Exception as e:
        logging.error("Erreur lors du chargement du modèle LDA:", e)
        return None





def get_topics(lda_model: LdaModel, num_topics: int):
    """
    Récupérer les topics du modèle LDA.
    Args:
        lda_model (LdaModel): Modèle LDA.
        num_topics (int): Nombre de topics à récupérer.

    Returns:
        list: Liste des topics.
    """
    topics = lda_model.show_topics(num_topics=num_topics, formatted=False)
    return topics



#gensimvis
def html_viz(lda_model: LdaModel,corpus,dictionary, vis_path: str):
    """
    Générer une visualisation HTML du modèle LDA.
    Args:
        lda_model (LdaModel): Modèle LDA.
        num_topics (int): Nombre de topics à récupérer.
        vis_path (str): Chemin vers le fichier HTML de sortie.
    """
    try:
        import pyLDAvis.gensim_models
        vis = pyLDAvis.gensim_models.prepare(topic_model=lda_model,corpus=corpus,dictionary=dictionary, sort_topics=False)
        pyLDAvis.save_html(vis, vis_path)
        logging.info("Visualisation HTML générée avec succès.")
    except Exception as e:
        logging.error("Erreur lors de la génération de la visualisation HTML:", e)
        return None



def main():
    """
    Fonction principale.
    """
    model_path = "models/"

    lda_model = load_model(model_path + "model.lda")


    corpus = Corpus.deserialize(model_path + "corpus.lda-c")

    dicts= load_dictionary(model_path + "dictionary.lda-d")


    vis_path = "HTML/lda_model.html"

    html_viz(lda_model,corpus,dicts, vis_path)


    


if __name__ == "__main__":
    main()