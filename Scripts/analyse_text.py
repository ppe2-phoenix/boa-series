#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from bao2 import process_text


def main():
    parser = argparse.ArgumentParser( description="analyse automatique des données")
    parser.add_argument("-t",'--text' ,nargs='?',  help="le texte à analyser")  
     
    args = parser.parse_args()

    if args.text:
        text = args.text
    else:
        parser.error("le texte à analyser est obligatoire")

    
    print("======================================")
    print("test analyse automatique des données")


    for token in process_text(text):
        for tag,content in token.items():
            print(f'{tag}:{content}')

if __name__ == '__main__':
    
    main()