#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BàO 3: analyse

1 - Topic modeling
2 - analyse dans le temps

"""
import logging
from typing import List
import pandas as pd
import xml.etree.ElementTree as ET
from gensim import corpora
from gensim.models import LdaModel
from gensim.models.coherencemodel import CoherenceModel
from gensim.models.phrases import Phrases, Phraser



def parse_file(path_file: str) -> List[dict]:
    """
    Charger le fichier XML du corpus et analyser son contenu.

    Args:
        path_file (str): Chemin du fichier XML.

    Returns:
        List[dict]: Liste de dictionnaires contenant les informations extraites du fichier.
    """

    try:
        # Charger le fichier XML
        tree = ET.parse(path_file)
        root = tree.getroot()

        data = []
        for file in root.findall('file'):
            categorie = file.attrib['categorie']
            
            
            articles = []
            pubDate = None
            for article in file.findall('article'):
                for element in article.findall('.//element'):
                    # Extraire les informations de chaque élément dans l'article
                    articles.append(tuple(element.attrib.values()))
                # Extraire la date de publication de l'article
                # convertir la date en format datetime 'yyyy-mm-dd'
                pubDate = pd.to_datetime(article.attrib['pubDate']).date()
                

            
            # Ajouter les informations extraites dans la liste de données
            data.append({'categorie': categorie,
                        'articles': articles, 'pubDate': pubDate})
        return data
    except ET.ParseError as e:
        logging.error("Erreur de parsing:", e)
        return None


def load_corpus(path_file: str, type='lemma', stop_words=None, no_pos: List[str] = None):
    """
    Charger le fichier XML du corpus, prétraiter les données et les stocker dans un DataFrame.

    Args:
        path_file (str): Chemin du fichier XML.
        type (str): Type de données textuelles à utiliser, 'word' ou 'lemma'. Par défaut, 'lemma'.
        stop_words (bool): Indicateur pour supprimer les mots vides. Par défaut, None.
        no_pos (List[str]): Liste des parties du discours à exclure. Par défaut, None.

    Returns:
        pd.DataFrame: DataFrame contenant les données prétraitées du corpus.
    """

    # Créer un dictionnaire pour mapper le type de données textuelles
    types = {'word': 0, 'lemma': 1}
    i = types[type]

    columns = ['categorie', 'articles', 'pubDate']
    data = parse_file(path_file)

    # Créer un DataFrame à partir des données extraites du fichier XML
    df = pd.DataFrame(data, columns=columns)

    # Prétraiter les données textuelles dans la colonne 'articles'
    df["articles"] = df["articles"].apply(lambda x: [str(item[i]).lower() for item in x
                                                     if (stop_words is None or (stop_words != True or item[3] == 'false'))
                                                     and (no_pos is None or item[2] not in no_pos)
                                                     and len(item[i]) > 3])

    return df


class Corpus(object):
    """
    Classe Corpus pour transformer les données textuelles en format de corpus utilisable par Gensim.
    """
    
    def __init__(self, data, dictionary):
        self.data = data
        self.dictionary = dictionary
        

    def __iter__(self):
        """
        Transformer les données textuelles en format de corpus utilisable par Gensim.

        Returns:
            iterable: Corpus Gensim.
        """
        for doc in self.data:
            #print(doc)
            yield self.dictionary.doc2bow(doc)

    def serialize(self,path):
        """
        Sérialiser le corpus Gensim dans un format binaire pour enregistrer sur le disque.
        Returns:
            str: Chemin du fichier binaire du corpus sérialisé.
        """
        try:
            
            corpora.BleiCorpus.serialize(path, self, id2word=self.dictionary)
            logging.info("Corpus Gensim sérialisé avec succès.")
            return path

        except Exception as e:
            logging.error(
                "Erreur lors de la sérialisation du corpus Gensim:", e)
            return None


def dictionary_builder(data):
    """
    Construire un dictionnaire Gensim à partir des données textuelles."""

    try:
        dictionary = corpora.Dictionary(data)
        logging.info("Dictionnaire Gensim construit avec succès.")

        # Supprimer les mots qui apparaissent une seule fois dans le corpus
        #bad_ids = [tokenid for tokenid,
        #           docfreq in dictionary.dfs.items() if docfreq == 1]
        

        #print(bad_ids)
        #dictionary.filter_tokens(bad_ids=bad_ids)

        # Supprimer les mots qui apparaissent dans moins de 20 documents
        # ou dans plus de 50% des documents
        #dictionary.filter_extremes(no_below=20, no_above=0.5)

        dictionary.compactify()

        return dictionary
    except Exception as e:
        logging.error(
            "Erreur lors de la construction du dictionnaire Gensim:", e)
        return None


def train_lda_model(corpus, id2word , num_topics=10, passes=10, iterations: int = 50):
    """
    Entraîner un modèle LDA (Latent Dirichlet Allocation) à partir d'un corpus.
    Args:
        corpus (iterable): Corpus Gensim.
        num_topics (int): Nombre de sujets pour le modèle LDA. Par défaut, 10.
        passes (int): Nombre de passages d'entraînement du modèle LDA. Par défaut, 10.

    Returns:
        gensim.models.ldamodel.LdaModel: Modèle LDA entraîné.
    """
    try:
        lda_model = LdaModel(corpus, id2word=id2word,num_topics=num_topics,
                             passes=passes, iterations=iterations,eta='auto',alpha='auto')
        logging.info("Modèle LDA entraîné avec succès.")
        return lda_model
    except Exception as e:
        logging.error("Erreur lors de l'entraînement du modèle LDA:", e)
        return None


def calculate_coherence(model, texts, dictionary):
    """
    Calculer la cohérence d'un modèle LDA.
    Args:
        model (gensim.models.ldamodel.LdaModel): Modèle LDA.
        texts (List[List[str]]): Liste des textes prétraités du corpus.
        dictionary (gensim.corpora.dictionary.Dictionary): Dictionnaire Gensim.

    Returns:
        float: Score de cohérence du modèle.
    """
    try:
        coherence_model = CoherenceModel(
            model=model, texts=texts, dictionary=dictionary, coherence='u_mass')
        coherence_score = coherence_model.get_coherence()
        logging.info("Score de cohérence calculé avec succès.")
        return coherence_score
    except Exception as e:
        logging.error("Erreur lors du calcul du score de cohérence:", e)
        return None


def get_topics(model, num_topics=10, num_words=10):
    """
    Obtenir les mots-clés pour chaque sujet d'un modèle LDA.
    Args:
        model (gensim.models.ldamodel.LdaModel): Modèle LDA.
        num_topics (int): Nombre de sujets pour le modèle LDA. Par défaut, 10.
        num_words (int): Nombre de mots-clés à extraire pour chaque sujet. Par défaut, 10.

    Returns:
        List[List[str]]: Liste des mots-clés pour chaque sujet.
    """
    try:
        topics = []
        for topic_id in range(num_topics):
            words = model.show_topic(topic_id, topn=num_words)
            topic = [word[0] for word in words]
            topics.append(topic)
        return topics
    except Exception as e:
        logging.error("Erreur lors de l'extraction des mots-clés:", e)
        return None


def bigram_count(worda_count, wordb_count, bigram_count, len_vocab, min_count, corpus_word_count):
    """
    Calculer le score de la mesure de bigramme.
    """
    score = (bigram_count * corpus_word_count) / ((worda_count * wordb_count) + min_count)
    return score

def process_model_train(corpus_file: str, stop_words: bool = True, no_pos: List[str] = None, num_topics: int = 10, passes: int = 10, iterations: int = 50,model_dir="models/"):

    # Charger le corpus

    df = load_corpus(corpus_file, type='lemma',
                     stop_words=stop_words, no_pos=no_pos)
    


    data = df['articles'].to_list()

    bigram = Phrases(data, min_count=5, threshold=1,scoring=bigram_count) # higher threshold fewer phrases.

    #print(list(bigram[data]))

    

    phrases = Phraser(bigram)

    

    

    bigram_mod = list(phrases[data])

    #print(bigram_mod)

    # Créer le dictionnaire
    dictionary = dictionary_builder(bigram_mod)

    

    if dictionary is None:
        logging.error("Impossible de créer le dictionnaire.")
        return
    else:
        logging.info("Dictionnaire Gensim créé avec succès. ")

    dict_path = save_dictionary(dictionary, model_dir + "dictionary.lda-d")

    if dict_path is None:
        logging.error("Impossible de sauvegarder le dictionnaire.")
        return
    else:
        logging.info("Dictionnaire sauvegardé avec succès. Chemin: {}".format(dict_path))
    




    # Créer le corpus Gensim
    corpus = Corpus(bigram_mod, dictionary)

    if corpus is None:
        logging.error("Impossible de créer le corpus Gensim.")
        return

    # Sérialiser le corpus
    corpus_path = corpus.serialize(model_dir + "corpus.lda-c")

    if corpus_path is None:
        logging.error("Impossible de sérialiser le corpus.")
        return
    else:
        logging.info("Corpus sérialisé avec succès. Chemin: {}".format(corpus_path))
    
    

    # Entraîner le modèle LDA
    lda_model = train_lda_model(
        corpus,dictionary, num_topics=num_topics, passes=passes, iterations=iterations)
    
    if lda_model is None:
        logging.error("Impossible d'entraîner le modèle LDA.")
        return

    # save model
    try:
        lda_model.save(model_dir +'model.lda')
        logging.info("Modèle LDA sauvegardé avec succès. Chemin: {}".format(model_dir +'model.lda'))
    except Exception as e:
        logging.error("Erreur lors de la sauvegarde du modèle LDA:", e)

    # Calculer le score de cohérence du modèle LDA
    coherence_score = calculate_coherence(
        lda_model, df['articles'], dictionary)
    
    if coherence_score is None:
        logging.error("Impossible de calculer le score de cohérence.")
        return
    
    logging.info(
        "Score de cohérence du modèle LDA: {}".format(coherence_score))
    
    """
    # Obtenir les mots-clés pour chaque sujet
    topics = get_topics(lda_model, num_topics=num_topics, num_words=10)

    if topics is None:
        logging.error("Impossible d'obtenir les mots-clés.")
        return
    
    logging.info("Mots-clés pour chaque sujet:")
    for topic_id, topic in enumerate(topics):
        logging.info("Sujet {}: {}".format(topic_id, topic))

    """


def save_dictionary(dictionary, dictionary_path):
    """
    Sauvegarder un dictionnaire Gensim.
    Args:
        dictionary (gensim.corpora.dictionary.Dictionary): Dictionnaire Gensim.
        dictionary_path (str): Chemin vers le dictionnaire.
    """
    try:
        dictionary.save(dictionary_path)
        logging.info("Dictionnaire sauvegardé avec succès.")
        return dictionary_path
    except Exception as e:
        logging.error("Erreur lors de la sauvegarde du dictionnaire:", e)
        return None





def main():
    # Configurer les paramètres de logging
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)s %(message)s')

    corpus_file = 'test/bao2_output.xml'


    no_pos = ['DET', 'PUNCT', 'SYM', 'ADP', 'AUX','CCONJ', 'PRON', 'NUM', 'PART', 'SCONJ']

    process_model_train(corpus_file=corpus_file, stop_words=True,
                        no_pos=no_pos, num_topics=10, passes=10, iterations=50)
    



if __name__ == "__main__":
    main()
