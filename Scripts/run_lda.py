#!/usr/bin/env python
# -*- coding: utf-8 -*-


from typing import Dict, List
import logging
from bao3 import process_model_train

from pprint import pprint
import argparse
from pathlib import Path
import os







    



def main():
    """
    Fonction principale

    """

    parser = argparse.ArgumentParser( description="Topic modeling avec Gensim"
                                     , epilog="Exemple d'utilisation: python3 script.py -p corpus_bao2.xml -nt 10 -sw -np VERB|AUX -v" )
    parser.add_argument("-p",'--path' ,nargs='?',   type=Path,help="le corpus XML à traiter")  
    parser.add_argument('-nt', '--num_topics', nargs='?', default=10, help="nombre de topics (10 par défaut)")
    parser.add_argument('-i', '--iterations', nargs='?', default=100, help="nombre d'itérations (100 par défaut)")
    parser.add_argument('-ps', '--passes', nargs='?', default=10, help="nombre de passes (10 par défaut)")
    parser.add_argument('-sw', '--stop_words', action='store_true', help="suppression des stop words (False par défaut)")
    parser.add_argument('-np', '--no_pos', nargs='?', default=None, help="liste des POS à supprimer (None par défaut)") 
    parser.add_argument('-v', '--verbose', action='store_true', help="afficher les messages de logging")   

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
        logging.info("Affichage des messages de logging")   
  
    if args.path:
        
        path_files = args.path

        #test si le chemin existe
        if os.path.exists(path_files):
            
            path_files =str(path_files)
           
        else:
            parser.error(f"le fichier '{args.path}' n'existe pas")
    else:
        parser.print_help()
        exit(1)
    
    if args.num_topics:
        num_topics = args.num_topics
    
    if args.iterations:
        iterations = args.iterations

    if args.passes:
        passes = args.passes

    if args.stop_words:
        stop_words = args.stop_words

    if args.no_pos:
        no_pos = args.no_pos.split('|')

    #stop_words = True
    no_pos = ['DET', 'PUNCT', 'SYM', 'ADP', 'AUX','CCONJ', 'PRON', 'NUM', 'PART', 'SCONJ']


    if not os.path.exists("models"):
        os.makedirs("models")
        




    process_model_train(corpus_file=path_files, stop_words=stop_words,
                        no_pos=no_pos, num_topics=num_topics, passes=passes, iterations=iterations)
    






if __name__ == '__main__':
    main()