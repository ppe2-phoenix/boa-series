#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
BàO 1: obtenir des données à partir d'un fichier RSS

1 - Lire un fichier RSS
2 - Extraction (récursive) de texte et de métadonnées

"""

import xml.etree.ElementTree as ET
import re
import feedparser
from typing import List, Dict
from pathlib import Path
import sys
from datetime import datetime
import json

#dictionnaire des codes de catégories
codes = {'une':'0,2-3208,1-0,0',
         'international':'0,2-3210,1-0,0',
         'europe':'0,2-3214,1-0,0',
         'societe':'0,2-3224,1-0,0',
         'idees':'0,2-3232,1-0,0',
         'economie':'0,2-3234,1-0,0',
         'actualite-medias':'0,2-3236,1-0,0',
         'sport':'0,2-3242,1-0,0',
         'planete':'0,2-3244,1-0,0',
         'culture':'0,2-3246,1-0,0',
         'livres':'0,2-3260,1-0,0',
         'cinema':'0,2-3476,1-0,0',
         'voyages':'0,2-3546,1-0,0',
         'technologies':'0,2-651865,1-0,0',
         'politique':'0,57-0,64-823353,0',
         'sciences':'env_sciences'

}


#dictionnaire des catégories
categories = { value: key for key, value in codes.items()}


def etree_parse_rss(path_file: str, tags:List[str] = ['title', 'description']) -> Dict[str, str]:
    """
    permettre la lecture d'un fichier rss avec l'utilisation de ElementTree
    retourner un dictionnaire avec les clés données en paramètre

    exemple d'utilisation:
    for item in etree_parse_rss('rss.xml', ['title', 'description']):
        print(item)

    args:
        path_file: chemin du fichier rss
        tags: liste des tags à extraire
        
    """
    try:
        tree = ET.parse(path_file)
    except ET.ParseError as e:
        #print("erreur de parsing", e)
        return None

    
    root = tree.getroot()

    for channel in root:
        for item in channel.findall('item'):
            yield {child.tag:child.text for child in item if child.tag in tags}

def clean_cdata(content: str) -> str:
    """
    permettre de nettoyer le contenu d'un tag CDATA

    exemple d'utilisation:
    print(clean_cdata('<![CDATA[<p>contenu</p>]]>'))

    args:
        content: contenu du tag CDATA
    """
    return re.sub(r'<!\[CDATA\[(.*?)\]\]>', r'\1', content)
    

def re_parse_rss(path_file: str, tags:List[str] = ['title', 'description']) -> Dict[str, str]:
    """
    permettre la lecture d'un fichier rss avec l'utilisation de regex
    retourner un dictionnaire avec les clés données en paramètre

    exemple d'utilisation:
    for item in re_parse_rss('rss.xml', ['title', 'description']):
        print(item)
    
    args:
        path_file: chemin du fichier rss
        tags: liste des tags à extraire
        

    """
    
    with open(path_file, 'r') as f:
        content = f.read()
        #parcourir les items
        for item in re.findall(r'<item>(.*?)</item>', content, re.DOTALL):
            #parcourir les tags
            yield {tag:clean_cdata(re.search(r'<%s>(.*?)</%s>' % (tag, tag), item, re.DOTALL).group(1)) for tag in tags}

def feedparser_parse_rss(path_file: str, tags:List[str] = ['title', 'description']) -> Dict[str, str]:
    """
    permettre la lecture d'un fichier rss avec l'utilisation de feedparser
    retourner un dictionnaire avec les clés données en paramètre

    """
    feed = feedparser.parse(path_file)
    for entry in feed.entries:
        yield {tag:entry[tag] for tag in tags}


def browse_files(path: Path, patterns: any,start_date:datetime=None ,end_date:datetime=None, file_ext: str = '.xml') -> Path:
    """
    parcouris de manière récursive un répertoire et retourne la liste des fichiers avec une liste de patterns

    exemple d'utilisation:
    for file in browse_files(Path('Corpus/2022'), ['0,2-3208,1-0,0']):
        print(file)
    
    args:
        path: chemin du répertoire
        patterns: liste des patterns à rechercher
        start_date: date de début de recherche
        end_date: date de fin de recherche
        file_ext: extension des fichiers à rechercher (par défaut .xml)
    
    """
    #print("parcours de ", path)
    
    regex = re.compile('|'.join(patterns))

    #parcourir les fichiers
    for sub in sorted(path.iterdir()):
        #print(sub, sub.stem)

        #si le fichier est un répertoire, on parcours récursivement
        if sub.is_dir():
            yield from browse_files(path=sub, patterns=patterns, start_date=start_date, end_date=end_date, file_ext=file_ext)

        #si c'est un fichier et que son extension et sa catégorie sont conformes à ce qui est recherché
        elif sub.suffix == file_ext and regex.search(sub.name):

            #extraction de la date à partir du nom du fichier (le chemin)
            file_date = datetime.strptime('-'.join(str(sub).split('/')[1:4]), "%Y-%b-%d")

            #si la date du fichier est comprise entre les dates de début et de fin, on retourne le fichier
            if start_date is not None and file_date < start_date:
                continue
            if end_date is not None and file_date > end_date:
                continue
            #print(">>fichier trouvé: ", sub)    
            yield sub


def extract_rss_to_xml(path_file: str, codes_categories:List[str], tags:List[str] = ['title', 'description'],start_date:datetime=None ,end_date:datetime=None, func_parser=etree_parse_rss, output=None) -> None:
    """
    permettre l'extraction des données d'un fichier rss vers un fichier xml

    exemple d'utilisation:
    extract_rss_to_xml('rss.xml', ['0,2-3208,1-0,0'], ['title', 'description'], start_date=datetime(2021, 1, 1), end_date=datetime(2021, 1, 5))

    args:
        path_file: chemin du fichier rss
        codes_categories: liste des codes des catégories à extraire
        tags: liste des balises RSS à extraire
        start_date: date de début de recherche
        end_date: date de fin de recherche
        func_parser: fonction de parsing à utiliser
        output: fichier de sortie (par défaut stdout)

    """

    # Créer l'élément racine du document XML
    root = ET.Element("corpus")

    # Créer l'élément de requête
    query = ET.SubElement(root, "query")
    ET.SubElement(query, "corpus_path").text = str(path_file)
    ET.SubElement(query, "start_date").text = str(start_date)
    ET.SubElement(query, "end_date").text = str(end_date)
    ET.SubElement(query, "categories").text = '|'.join([categories[c] for c in codes_categories])

    # Parcourir les fichiers
    fnum = 0
    for in_rss in browse_files(Path(path_file), codes_categories, start_date, end_date):
        fnum += 1
        cat = categories[in_rss.stem]
        file_elem = ET.SubElement(root, 'file', categorie=cat, num=str(fnum), filename=str(in_rss))

        
        num = 0
        for item in func_parser(in_rss, tags):
            num += 1
            article_elem = ET.SubElement(file_elem, 'article', num=str(num), categorie=cat)
            for tag, value in item.items():
                if tag in ['title', 'description']:                                 
                    ET.SubElement(article_elem, tag).text = value
                elif tag == 'pubDate':
                    pubDate = datetime.strptime(value, "%a, %d %b %Y %H:%M:%S %z").isoformat()
                    article_elem.set('pubDate', pubDate)
                else:
                    ET.SubElement(article_elem, tag).text = value

    xml_str = ET.tostring(root, encoding='utf-8', xml_declaration=True)
    #xml_str = xml_str.
    if output is not None:        
        with open(output, 'wb') as f:
            f.write(xml_str)
    else:
        print(xml_str.decode('utf-8'))





def extract_rss_to_json(path_file: str, codes_categories:List[str], tags:List[str] = ['title', 'description'],start_date:datetime=None ,end_date:datetime=None, func_parser=etree_parse_rss, output=None) -> None:
    """
    permettre l'extraction des données d'un fichier rss vers un fichier json

    exemple d'utilisation:
    extract_rss_to_json('rss.xml', ['0,2-3208,1-0,0'], ['title', 'description'], start_date=datetime(2021, 1, 1), end_date=datetime(2021, 1, 5))

    args:
        path_file: chemin du fichier rss
        codes_categories: liste des codes des catégories à extraire
        tags: liste des balises RSS à extraire
        start_date: date de début de recherche
        end_date: date de fin de recherche
        func_parser: fonction de parsing à utiliser
        output: fichier de sortie

    """

     # Création du dictionnaire qui contiendra les données extraites
    rss_data = {
        "query": {
            "corpus_path": str(path_file),
            "start_date": start_date.isoformat(),
            "end_date": end_date.isoformat(),
            "categories": "|".join([categories[c] for c in codes_categories])
        },
        "files": []
    }
    
    # Parcours des fichiers
    fnum = 0
    for in_rss in browse_files(Path(path_file), codes_categories, start_date, end_date):
        categorie = categories[in_rss.stem]
        file_data = {
            "numFile": fnum + 1,
            "categorie": categorie,
            "filename": str(in_rss),
            "articles": []
        }

        num = 0
        for item in func_parser(in_rss, tags):
            article_data = {
                "numArticle": num + 1,
                "categorie": categorie
            }

            for tag, value in item.items():
                if tag == 'pubDate':
                    article_data[tag] = datetime.strptime(value, "%a, %d %b %Y %H:%M:%S %z").isoformat()
                else:
                    article_data[tag] = value

            file_data["articles"].append(article_data)
            num += 1

        rss_data["files"].append(file_data)
        fnum += 1

    # Écriture du dictionnaire dans le fichier JSON
    if output is not None:
        with open(output, 'w') as json_file:
            json.dump(rss_data, json_file, indent=4)
    else:
        print(json.dumps(rss_data, indent=4))

#fonction de parsing
parser_rss = {
    'etree': etree_parse_rss,
    're': re_parse_rss,
    'feedparser': feedparser_parse_rss
}

#fonction d'extraction
extract_rss_to = {
    'xml': extract_rss_to_xml,
    'json': extract_rss_to_json
}




def main():

    path_file = 'Corpus/2022'

    date1 = datetime(2022, 1, 1)
    date2 = datetime(2022, 1, 2)

    funct_parser = parser_rss['etree']

    extract_rss = extract_rss_to['json']


    codes_categories = [ v for k,v in codes.items() if k in [ 'sport','sciences']]
    #print(codes_categories)

    #output = 'test_xml.xml'
    #test de la fonction browse_files et extract_rss_to_xml
    #extract_rss_to_xml(path_file, codes_categories=codes_categories,tags=['title', 'description','pubDate'], start_date=date1, end_date=date2,func_parser=funct_parser, output=output)
    

    #test de la fonction browse_files et extract_rss_to_json
    output = 'sortie_extraction.json'
    extract_rss(path_file, codes_categories=codes_categories,tags=['title', 'description','pubDate'], start_date=date1, end_date=date2,func_parser=funct_parser, output=output)

    
    sys.exit() 



if __name__ == '__main__':
    
    main()
