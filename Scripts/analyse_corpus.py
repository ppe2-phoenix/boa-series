#!/usr/bin/env python
# -*- coding: utf-8 -*-


import argparse
import os
from pathlib import Path
from bao2 import process_xml


def main():
    parser = argparse.ArgumentParser(
        description="analyse automatique des données")
    parser.add_argument("-p", '--path', nargs='?',
                        help="le fichier XML à analyser")
    parser.add_argument('-o', '--output', nargs='?', default='stdout',
                        help="fichier de sortie (stdout par défaut)")
    args = parser.parse_args()

    if args.path:
        path_file = args.path

        # test si le chemin existe
        if os.path.exists(path_file):
            # test s'il s'agit d'un répertoire
            path_file = Path(path_file)

        else:
            parser.error(f"le fichier '{args.path}' n'existe pas")
    else:
        parser.error(f"le fichier XML à analyser n'est pas spécifié")

    output = None

    if args.output:
        if args.output != 'stdout':
            output = args.output

    #
    process_xml(path_file,  output=output)




if __name__ == '__main__':

    main()
