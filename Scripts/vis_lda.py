#!/usr/bin/env python
# -*- coding: utf-8 -*-


from typing import Dict, List
import logging
from bao4 import load_model, load_dictionary, Corpus, html_viz

from pprint import pprint
import argparse
from pathlib import Path


    



def main():
    """
    Fonction principale

    """

    parser = argparse.ArgumentParser( description="Topic visualization avec pyLDAvis"
                                     , epilog="Exemple d'utilisation: python3 script.py -p models/ -o myvisLDA.html -c -v" )
    parser.add_argument("-p",'--path' ,nargs='?', default="models/",  type=Path,help="model LDA à visualiser (par défaut: models/))")  
    parser.add_argument('-c', '--coherence',action="store_true", default=False, help="afficher la cohérence des topics (False par défaut)")
    parser.add_argument('-v', '--verbose', action='store_true',default=False, help="afficher les messages de logging")
    parser.add_argument('-o', '--output', nargs='?', default="myvisLDA.html", help="nom du fichier de sortie (myvisLDA.html par défaut)")


    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
        logging.info("Affichage des messages de logging")   
  
    if not args.path:
        parser.print_help()
        exit(1)
    else:
        model_path = args.path


    
    model_path =str(model_path) +"/"
     
    
    if args.output:
        output = args.output
    else:
        output = None

    if args.coherence:
        coherence = True    
    else:
        coherence = False

    mLDA = load_model(model_path + "model.lda")

    corpus = Corpus.deserialize(model_path + "corpus.lda-c")

    dicts= load_dictionary(model_path + "dictionary.lda-d")

    if output:
        vis_path = output
        html_viz(mLDA,corpus,dicts, vis_path)

    
    if coherence:
        coherence = mLDA.coherence
        print("Coherence des topics: ", coherence)
        


if __name__ == '__main__':
    main()